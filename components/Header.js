import Head from "next/head";
import 'bootstrap/dist/css/bootstrap.css'

export default function Header({ children, title }){
    return (<div>
        <Head>
            <title>Prueba Tecnica - Hernan Gonzalez</title>
            <meta name="description" content="Prueba tecnica, capa web, realizada en next.js" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
        </Head>
        <div>
            {children}
        </div>
    </div>
    );
}