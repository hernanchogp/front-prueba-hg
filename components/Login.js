import { validateUser } from '../services/Login';


export default function Login() {
  // Handles the submit event on form submit.
  const handleSubmit = async (event) => {
    // Stop the form from submitting and refreshing the page.
    event.preventDefault()

    // Get data from the form.
    const data = {
      usuario: event.target.usuario.value,
      clave: event.target.pass.value,
    }
    const response = await validateUser(data);
    if (response.codigo != '0000') {
      alert(response.msj);
    } else {
      sessionStorage.setItem("session", 'ok');
      location.reload();
    }
  }
  return (
    <div className="limiter">
      <div className="container-login100">

        <div className="wrap-login100">
          <div className="login100-pic js-tilt" data-tilt="">
            <img src="../img/logo.png" alt="IMG" />
          </div>
          <form className="login100-form" onSubmit={handleSubmit}>
            <span className="login100-form-title">Gestion Información Empleados</span>
            <div
              className="wrap-input100"
            >
              <input
                className="input100"
                type="text"
                name="usuario"
                id="usuario"
                placeholder="Usuario"
                data-required='SI'
                required='required'
              />
              <span className="focus-input100" />
              <span className="symbol-input100">
                <i className="fa fa-envelope" aria-hidden="true" />
              </span>
            </div>
            <div className="wrap-input100 ">
              <input
                className="input100"
                type="password"
                name="pass"
                id="pass"
                placeholder="Password"
                data-required='SI'
                required='required'
              />
              <span className="focus-input100" />
              <span className="symbol-input100">
                <i className="fa fa-lock" aria-hidden="true" />
              </span>
            </div>
            <div className="container-login100-form-btn">
              <button type='submit' className='login100-form-btn'>
                Login
              </button>

            </div>
          </form>
        </div>

      </div>
    </div>
  )
}
