import 'bootstrap/dist/css/bootstrap.css'

const Footer = () => (
    <footer className="bg-light text-center text-lg-start">
        <div className="text-center p-3" style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
            © 2022 Copyright: &nbsp;
            <span className="text-dark">Presentado por Hernan Gonzalez</span>
        </div>
    </footer>
)

export default Footer;