import styles from '../styles/Home.module.css'
import Header from '../components/Header';
import Footer from '../components/Footer';
import Body from '../components/Body';
import 'bootstrap/dist/css/bootstrap.css'

export default function Home() {
  
  return (
    <body >
      <div className={styles.container}>
        <Header></Header>

        <main >

          <Body></Body>


        </main>

        <Footer></Footer>
      </div>
    </body>
  )
}
