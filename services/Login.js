export async function validateUser(params) {

    const endpoint = process.env.NEXT_PUBLIC_ENDPOINT_API + '' + 'usuario';
    const JSONdata = JSON.stringify(params)
    const options = {
        // The method is POST because we are sending data.
        method: 'POST',
        // Body of the request is the JSON data we created above.
        body: JSONdata
    }

    try {        
        const request = await fetch(endpoint, options)      
        const result = await request.json()   
        return result;
    } catch (error) {
        console.log('Fetch error: ', error);
    }

}
